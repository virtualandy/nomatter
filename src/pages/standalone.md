---
title: Going Serverless
date: "2018-08-29"
---

Fun news from GopherCon - [running Go in GCP Cloud Functions is apparently in alpha](https://github.com/kelseyhightower/gophercon-2018).

More to come from GCP soon I'm sure.