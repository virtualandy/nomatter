---
title: Rands and the Second Act
date: "2018-09-27"
---

## I <3 Rands

When I saw that Michael Lopp, aka [Rands in Repose](https://randsinrepose.com) was going to be at Denver Startup Week (DSW) 2018, I was stoked.
I knew I had read his blog for a while - when I went back and looked at [my pinboard](https://pinboard.in/u:virtualandy) I found a bookmarked post from 2011. And that's probably a year or two off (I must've only started using Pinboard back then).

I'm not a regular reader and I sadly haven't gotten thru his books, but I like his writing style and topics of choice and have learned quite a bit from that blog.

### DSW

I did make it to two of his events at DSW, one a roundtable and the other a talk on "The Second Act". I took notes, which I'll share below.

You can watch a [video of the talk given at another event (Youtube)](https://www.youtube.com/watch?v=rrOSvBZwVR4) if you would rather watch and not read. (And I don't blame you, these notes need a bit of cleanup.)

### Notes in the raw

Lopp - The Second Act
DSW 9/26
Improper City

Two apologies - 
What a yard sale of mediocrity in terms of products (HP, IBM, MSFT)
Wildly successful companies - because they’ve gone on to the second act

Understand what was built in the first act
What does winning look like
Tips for a successful 2nd act

9/10 startups fail - just getting to one product is great
If you made it to a product - welcome to winning!
AfterDark screensaver software made millions of dollars - flying toasters
1+1 to 30 to 300
At one person decisions are fast
Then you hire a person. Then there’s a discussion on decisions. That is the start of a culture.
Culture is like the force - but it’s real!

At 30 people ideas are coming from everywhere. Decisions remain collaborative and visible. Execution is shared. No time to bicker.

The old guard. Demographic built at this point in time.

Post 300
Stories are retold and become myth (from the old guard)
Situational awareness is becoming expensive
Increasing communication friction
Learning can no longer occur via osmosis - core frustration of old guard

The NEW guard then shows up.
And they quickly outnumber the old guard. They write things down, they define process. They import languages and customs from previous gigs.

The old guard believes the lessons of the past are definitive. Writing things down is a waste of time. Meetings suck.

This tension of old guard versus new guard is not winning.

#### What does winning look like
Building a company of humans capable of building and sustaining an entirely new product.

Why is this so hard?
A confusing omnipresent force that doesn’t believe the new and compelling can exist.

Life is like a ride - comedian Bill Hicks

Fundamental parts of the culture must be allowed to evolve to encourage the second act.

#### Tips for a successful second act

No short cuts - is a force of culture
The culture must evolve in uncomfortable ways.

The lessons that got you here will not get you there.
Process is documented culture but it must be able to defend itself. If someone can’t tell you why you’re supposed to follow a process, you don’t have to do it.

Encourage diversity of thought. You want people to disagree, challenge and give you good ideas. Ideas don’t get better with agreement.

What is the one aspect of your culture that is bugging you?
Destiny and leaders - DJ (the leader of 6) is unfailingly kind. “No worries”
Turn moments of despair into a teaching moment. There are always curveballs - don’t get frustrated. Figure it out.

Be unfailingly kind. 

There is no old guard or new guard. Those are labels. It’s your job to figure out how to get to the second act.

Question about flat hierarchy.
Don’t think it scales. Team sizes of 7 plus or minus 3 is important. Manager included. 3 levels of hierarchy from person running the show down to the lowest level. Capable of fanning out to 2500 that way. Strong leadership, small teams and flattish orgs.

“Engineering program manager” and “engineering operations” - amazing humans that connect silos. Phenomenal judgement. Strong signal network and execution.

How long does process building take? 
OKRs are a way to break apart silos. Structured prioritization of work.
Adapt process to the team. Don’t ram a process into the org.
Took 1.5 years and 3 tries for Slack to work through promotion process. Old guard will want to do it quicker but it takes time.

Health of an org is based on what you’re doing. If org is bored or not learning lessons, it might die. Can be successful with just one act/product.

Question on thesis and metrics. Have a hypothesis but might have to wait.

Sales engineer and needing to stay kind. You can be kind by saying something hard.

What about old guard that is resistant?
Sometimes they leave. Keeping them can be hard - they love 0-50. Some choose to evolve with the company.

How do you balance engineers complaining about founders coding?
Don’t get in the way of people. Don’t sign up for a feature if you’re the manager. Work the muscle.

How do you create a culture for the process team? 
Engineers need to see value - so make sure they have attributes and power to provide value. Learning culture and operationalize learnings. 

http://randsinrepose.com/archives/the-old-guard/
http://randsinrepose.com/archives/the-guard/

