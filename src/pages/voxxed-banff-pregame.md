---
title: Here we go, Voxxed Days
date: "2018-10-25"
---

## It's here!

Tomorrow is the start of [Voxxed Days Banff](https://voxxeddays.com/banff/). I'm incredibly thankful that I will be presenting and am almost as excited to hear all the other presentations as well. It should be a lot of fun and pretty educational - I'm especially interested in the Kafka and other event driven talks becasue I havre both a personal and professional interest and I want to learn more about those ideas and technologies.

One great way to learn? Practice. And lots of it.

If you're intersted in that, hopefully you'll see my talk. If aren't in Banff (or are and just can't make it), please find me in real life or online as I'd love to chat about how to apply the principles of deliberate practice in software development. Software engineering feels like it never stands still and one must constantly learn - I agree, but I'd also like to see the software community focus on not just learning enough to cut a pull request or put out a patch, but improving through continual practice as well.