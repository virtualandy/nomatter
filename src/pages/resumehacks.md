---
date: "2019-04-01"
title: "How to: Hack your resume"
---

In the age of Github, Stack Overflow and several ways to look at someone's
education or work history, resumes are still important.

More than once, I've been told "Thank you for giving me a one page resume."

Here's a few things I've learned as I've changed and updated [my resume](https://www.dropbox.com/s/dp0acddcfq5xgm1/AndyEnnamorato-2018.pdf?dl=0) over time.

### Keep it Short

Like a [great talk](https://www.gilderlehrman.org/content/gettysburg-address-1863), a short, succint and impactful resume can stand out.
(_Ed's note: My own writing is usually not short, or succinct, or impactful. My resume is not much better. :)_)

Write and then edit your resume several times to see how different wording or a different approach helps you describe what you did with less fluff.
Try [the Hemingway Editor](http://www.hemingwayapp.com) or similar to get feedback on your writing (or use a friend/coworker/live human).

I think one page is enough room and two pages more than enough.

### Hack the format

One of my favorite tricks is hacking the format - trying a different layout using 2 or 3 columns.
Shrink the margins and header/footer to give yourself more space. 
Adjust the font accordingly - go larger for headlines and important things. If you're writing a lot of info in a bulleted list, consider using a smaller font to give yourself more room.
Use both sides of the page if you're going to turn it in.

### Don't list every technology

Two reasons not to list every technology you've ever encountered or are interested in:

1. You might get interviewed or hired for a technology you no longer want to work in

2. You might get trivia questions about a technology you have long forgotten


If it no longer applies, don't be afraid to leave it out.

### Tailoring

If you have the opportunity (some job sites or resume upload tools won't support it), try to write a cover letter or letter of interest along with submitting your resume. Tailor it towards the job or company that you are applying for - "I've long wanted to work in ecommerce and SellsThings Inc. sounds like a great place. I think my skills would be a fit..." or similar can help you stand out from "I need a job" type applications.

Similarly, tailor your resume as well. Edit, add or remove information from your job descriptions or educational/side projects. If you are applying to an ecommerce company, highlight your experiences in all things billing and payments and consider removing or simplifying other sections.

Using an **Objective** section in your resume that is tailored to the job is great! It can be a detriment if you're applying to a lot of places (and trying to manage different applications) or need the space to highlight your skills.

### Keep the extracurriculars

Opinions are likely varied, but I personally like seeing (and using) extracurricular information on a resume. It can give a hiring team a bit of perspective ("This person is self taught - and they still take Coursera courses!") or fill in the blanks.
An excellent director I know loves seeing service industry experience - if you've had a job in a restuarant, coffee shop or retail, you've probably run into a lot of angry customers. Troubleshooting code might be a breeze in comparison.

## Resources

Here are some resources around resume writing that I like:
* [How to write a killer Software Engineering résumé](https://medium.freecodecamp.org/writing-a-killer-software-engineering-resume-b11c91ef699d): An in-depth analysis of the résumé that got me interviews at Google, Facebook, Amazon, Microsoft, Apple, and more. - Appreciate the author giving us the "before" and "after"

* [How to write an awesome junior developer résumé in a few simple steps](https://medium.freecodecamp.org/how-to-write-an-awesome-junior-developer-résumé-in-a-few-simple-steps-316010db80ec) - There's a lot of new folks joining the software industry, which is awesome. Helpful to have a perspective on what it is like as you're just getting started. I've forgotten a lot since I was in those shoes.

* [Hemingway App](http://www.hemingwayapp.com) - "Hemingway App makes your writing bold and clear." I really enjoy using Hemingway and similar tools to get realtime feedback on how and what (and why) I'm writing something. ( _Ed's note: This blog not yet filtered thru Hemighway lens._ )

* [How to prepare for a technical interview](https://medium.com/@dominicwhite/how-to-prepare-for-a-technical-interview-182e64dd95ae) - This blog from a longtime developer and hiring manager links to another post from the author on [how to write your resume](https://medium.com/@dominicwhite/i-hire-software-developers-your-resume-is-the-reason-youre-not-getting-interviews-dc7b2520a2f1).