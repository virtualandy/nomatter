---
date: "2018-09-01"
title: Welcome, September
---

I should know by now, but time flies.

<!-- end -->

The latter 2 children of mine have birthdays this month. So we're rather excited and planning on a good bit of 🎂 the next couple weeks.

My sister is also getting married soon and that should be fun - my whole family will be in town, a reunion of 9 siblings, two parents and whomever else makes it.

My friend and former coworker has been cranking on a side project and I hope to help him out in a better way, but with the above happening we'll see. 

Time flies when priorities are unclear. Committing time to this blog is a priority - but it absolutely comes second to birthdays and cake. 