---
title: Sleepless
meta: featured post
---

I work with one of the most interesting people in the world.

<!-- end -->

If you have time - and like cars that go really really fast - [you should read their blog about breaking speed records at the Bonneville Salt Flats](http://dammralliers.com/2018/08/what-a-long-strange-trip-its-been/).

As for me, it's early (late?) and I'm having trouble sleeping. Maybe I need a race car to crank on. 🤑