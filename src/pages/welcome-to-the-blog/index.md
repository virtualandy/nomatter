---
title: Welcome to the Blog!
date: "2018-08-27"
featuredImage: './featured.jpg'
---

This is my blog. There are many like it, but this one is mine.

## Why?

Practice writing. Practice Javascript/React. Learn Gatsby.

<!-- end -->

And because it's been a long time coming.
